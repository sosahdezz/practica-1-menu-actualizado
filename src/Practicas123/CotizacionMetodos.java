/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practicas123;

/**
 *
 * @author REDES
 */
public class CotizacionMetodos {

    private int numCotizacion;
    private String descripcionAuto;
    private float precio;
    private float porcentajePagoinicial;
    private int plazo;
    private float total;
    private float mensual;
    
    //CONSTRUCTORES 
    
    public CotizacionMetodos (){
        this.numCotizacion = 0;
        this.descripcionAuto = "";
        this.precio = 0.0f;
        this.porcentajePagoinicial = 0.0f;
        this.plazo = 0;
    }
    public CotizacionMetodos (int numCotizacion, String descripcionAuto, float precio, float porcentajePagoinicial, int plazo){
        this.numCotizacion = numCotizacion;
        this.descripcionAuto = descripcionAuto;
        this.precio = precio;
        this.porcentajePagoinicial = porcentajePagoinicial;
        this.plazo = plazo;
    }
    public CotizacionMetodos (CotizacionMetodos otro){
        this.numCotizacion = otro.numCotizacion;
        this.descripcionAuto = otro.descripcionAuto;
        this.precio = otro.precio;
        this.porcentajePagoinicial = otro.porcentajePagoinicial;
        this.plazo = otro.plazo;
    }
    //METODOS
    
    public int getnumCotizacion(){
        return numCotizacion; }
    public void setnumCotizacion(int numCotizacion){
        this.numCotizacion = numCotizacion;
    }
    public String getdescripcionAuto(){
        return descripcionAuto; }
    public void setdescripcionAuto(String descripcionAuto){
        this.descripcionAuto = descripcionAuto;
    }
    public float getprecio(){
        return precio; }
    public void setprecio( float precio){
        this.precio = precio;
    }
    public float getporcentajePagoinicial(){
        return porcentajePagoinicial; }
    public void setporcentajePagoinicial( float porcentajePagoinicial){
        this.porcentajePagoinicial = porcentajePagoinicial;
    }
     public int getplazo(){
        return plazo; }
    public void setplazo(int plazo){
        this.plazo = plazo;
    }
    
    //METODOS COMPORTAMIENTO
    public float pagoInicial (){
        float pago = 0.0f;
        pago = (this.precio * (this.porcentajePagoinicial/100));
        return pago;
    }
    
    public float totalFinanciar (){
        float total = 0.0f;
        float pago = 0.0f;
        pago = (this.precio * (this.porcentajePagoinicial/100));
        total = (this.precio - pago); //aqui creo que hay error
        return total;
    }
    
    public float pagoMensual (){
        float total = 0.0f;
       // float plazo = 0.0f;
        float pago = 0.0f;
        pago = (this.precio * (this.porcentajePagoinicial/100));
        total = this.precio - pago;
        
        float mensual = 0.0f;
        mensual = (total / this.plazo); //aqui el total no esta puesto en la clase 
        return mensual;
    }
}